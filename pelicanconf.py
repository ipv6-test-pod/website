# Pelican config
# See https://docs.getpelican.com/en/latest/settings.html#example-settings

AUTHOR = "jharr@internet2.edu"
SITENAME = "IPv6 Test Pod"
SITEURL = ""

TIMEZONE = "UTC"
DEFAULT_LANG = "en"

### Plugins and themes
THEME = "pelican-themes/pelican-bootstrap3"
PLUGIN_PATHS = [
    "pelican-plugins",
]
PLUGINS = [
    "i18n_subsites",
]
JINJA_ENVIRONMENT = {
    "extensions": ["jinja2.ext.i18n"],
}

### Content paths
PATH = "content"
PATH_METADATA = r"(pages/)?(?P<path_no_ext>.*)\..*"
# PAGE_PATHS = ["pages"]

# ARTICLE_PATHS = ["articles"] # default is ""
# ARTICLE_URL = "{date:%Y-%m}/{slug}"
# ARTICLE_SAVE_AS = "{date:%Y-%m}/{slug}/index.html"
ARTICLE_URL = "{path_no_ext}.html"
ARTICLE_SAVE_AS = "{path_no_ext}.html"
PAGE_URL = "{path_no_ext}.html"
PAGE_SAVE_AS = "{path_no_ext}.html"

MONTH_ARCHIVE_SAVE_AS = "{date:%Y-%m}/index.html"
MONTH_ARCHIVE_URL = "{date:%Y-%m}/index.html"

DELETE_OUTPUT_DIRECTORY = True

STATIC_PATHS = [
    "images",
    "files",
]

### Home page config
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False # Category links manually added below
MENUITEMS = (
    # ("About", "/about.html"),
    # ("Participate", "/participate.html"),
    ("Apply Now!", "https://forms.gle/CcD3QjJGmJw8y3ys9"),
    ("News", "/category/news.html"),
    ("Terms of Service", "/files/terms-of-service.pdf"),
)
LINKS = (
    ("ARIN Community Grants", "https://www.arin.net/about/community_grants/program/"),
    ("Internet2", "https://internet2.edu/"),
)

# Social widget
SOCIAL = (
    # ("Link title", "#"),
)

SUMMARY_MAX_LENGTH = None

#DISPLAY_RECENT_POSTS_ON_SIDEBAR = True
#DISPLAY_ARCHIVE_ON_SIDEBAR = True

DEFAULT_PAGINATION = False
# RELATIVE_URLS = True

### Feed generation
# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
