Title: IPv6 Test Pod
Date: 2024-01-29
save_as: index.html

## Introduction to the IPv6 Test Pod

As IPv6 usage continues to grow, it's important to ensure that software and services will function not only in a dual-stack environment, but also in IPv6-only networks. Not every network has access to IPv6 today, and on top of that, setting up an IPv6-only environment can be challenging, even for a network engineer.

Enter the IPv6 Test Pod - a device that aims to make it easy to test software and services in a variety of IPv6 networks including dual-stack, IPv6-only, and IPv6-only with transition mechanisms to access the IPv4-only internet, such as NAT64, DNS64, and PREF64.

The IPv6 Test Pod is a relatively inexpensive device made available at no cost that provides a variety of IPv6 test networks via Wifi SSIDs and optionally over Wired Ethernet connections. All you have to do is provide a wired internet connection to the WAN interface (IPv4-only is OK), power, and it will provide a series of networks to test devices and software with:

- Dual-Stack (IPv4 and IPv6)
- IPv6-only
- IPv6 only with NAT64+DNS64
- IPv6-only with NAT64+PREF64
- IPv6 only with NAT64+DNS64+PREF64

These devices will come at no cost to a participant. IPv6 Tunnel termination is provided as part of this project by Internet2.

## Who is this intended for?

This project

- Software developers wanting to test their software against IPv6-only networks.
- IT support personnel wanting to test systems and software they support in IPv6-only networks.
- Network Engineers wanting to test IPv6-only networks and IPv6-only transition technologies
  such as 464XLAT.

## How can I get one?

[Fill out the Application Form!][3] - We're now accepting applications and will review
submissions and ship out when the hardware is ready.

Sign up to the [announce mailing list][1] for updates.

## What happens when you run out of hardware?

If hardware runs out, we want to look at ways to distribute a VM image participants can run or
an OS image that can be flashed to hardware purchased by the user.

## Is there a cost?

There is no cost; This is made possible by the [ARIN Community Grant program][2].

[1]: https://lists.internet2.edu/sympa/info/ipv6-pod-announce
[2]: https://www.arin.net/blog/2023/09/11/Congratulations-2023-ARIN-Community-GrantRecipients/#ipv6-test-pod
[3]: https://forms.gle/CcD3QjJGmJw8y3ys9
