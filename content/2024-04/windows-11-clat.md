Title: Windows 11 to Expand CLAT Support
Date: 2024-04-07
Category: News

Microsoft has announced that it plans to expand CLAT support to other interface
types.

[Microsoft's Announcemnt in the Tech Community forum][1]:

> ... We are committing to expanding our CLAT support to include non-cellular network
> interfaces in a future version of Windows 11. This will include discovery using
> the relevant parts of RFC 7050 (ipv4only.arpa DNS query), RFC 8781 (PREF64 option
> in RAs), and RFC 8925 (DHCP Option 108) standards. Once we do have functionality
> available for you to test in Windows Insiders builds, we will let you know.

More information in the [announcement post][1] or [IPv6 Buzz Episode 148][2].

[1]: https://techcommunity.microsoft.com/t5/networking-blog/windows-11-plans-to-expand-clat-support/ba-p/4078173
[2]: https://packetpushers.net/podcasts/ipv6-buzz/ipb148-microsoft-to-expand-clat-support-in-windows-11/
